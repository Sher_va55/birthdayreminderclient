package com.ppjava13v2.vera.myapplication.db.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;


public class SocialUser implements Serializable {

    private long id;
    private String socialId;
    private Date birthday;

    @SerializedName("fio")
    private String FIO;

    public SocialUser(long id, String socialId, Date birthday, String FIO) {
        this.id = id;
        this.FIO = FIO;
        this.socialId = socialId;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
