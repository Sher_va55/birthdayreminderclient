package com.ppjava13v2.vera.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.*;

import com.facebook.*;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.*;
import com.ppjava13v2.vera.myapplication.services.SocialUserDataService;

import java.util.*;



public class FacebookActivity extends Activity {

    private String accessToken;
    private CallbackManager callbackManager;
    private SocialUserDataService socialUserDataService;
    private ShareDialog shareDialog;
    private String login;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.baloons);

        setContentView(R.layout.activity_facebook);

        Intent intent = getIntent();
        login = intent.getStringExtra("login");
        password = intent.getStringExtra("password");

        socialUserDataService =new SocialUserDataService(login,password);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        LoginButton loginButton= (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends", "user_birthday"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken().getToken();
                socialUserDataService.sendFacebookAccessToken(accessToken);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {
                Log.wtf("Error", "ErrorCallback");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onShareResult(View view) {

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("successShare", "success");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("errorShare", "error");
            }

            @Override
            public void onCancel() {
                Log.d("cancelShare", "cancel");
            }
        });

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Birthday Reminder")
                    .setContentDescription(
                            "My new application birthday reminder")
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build();

            shareDialog.show(linkContent);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        menuInflater.inflate(R.menu.menu_facebook, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.exit) {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
        if (item.getItemId()==R.id.back) {

            Intent intent=new Intent (this, FriendsBirthdayActivity.class);
            intent.putExtra("login",login);
            intent.putExtra("password",password);
            startActivity(intent);
            finish();
        }

        return false;
    }
}
