package com.ppjava13v2.vera.myapplication;

import android.content.Context;
import android.view.*;
import android.widget.*;

import com.ppjava13v2.vera.myapplication.db.entities.SocialUser;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Adapter for building ListView with custom view. View contains image and 2 text fields
 */
public class FileAdapter extends ArrayAdapter<SocialUser> {

    private Context context;
    private List<SocialUser> objects;

    public FileAdapter(Context context, int resource, List<SocialUser> objects) {
        super(context, resource, objects);
        this.context=context;
        this.objects=objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SocialUser socialUser = getItem(position);

        View currentView;
        currentView=
                convertView==null?
                        LayoutInflater.from(context).inflate (R.layout.viewforfriendslist,null):
                        convertView;

        TextView name = (TextView) currentView.findViewById(R.id.fio);
        TextView birthday = (TextView) currentView.findViewById(R.id.birthday);
        ImageView button = (ImageView) currentView.findViewById(R.id.photo);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String birthdayString = formatter.format(socialUser.getBirthday());

        name.setText(socialUser.getFIO());
        birthday.setText(birthdayString);

        return currentView;
    }
}
