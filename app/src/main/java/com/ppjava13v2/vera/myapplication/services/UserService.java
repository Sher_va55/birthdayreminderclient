package com.ppjava13v2.vera.myapplication.services;

import android.database.Cursor;

import com.ppjava13v2.vera.myapplication.db.DAO.UsersDAO;
import com.ppjava13v2.vera.myapplication.db.entities.Users;

/**
 * Provides operations with users
 */
public class UserService {

    private UsersDAO usersDAO;

    public UserService() {
        usersDAO= new UsersDAO ();
    }

    public long saveUser (String email,String login,String password) {
        Users user=new Users (email,login,password);
        return usersDAO.addUser(user);
    }

    public long deleteUser (long id) {
        return usersDAO.deleteUser(id);
    }

    public boolean checkPassword (String login,String password) {

        if (login.length ()!=0&&password.length()!=0) {
            Cursor cursor=usersDAO.findByLoginAndPassword(login,password);
            if (cursor.getCount()!=0) {
                return true;
            }
        }

        return false;
    }
}
