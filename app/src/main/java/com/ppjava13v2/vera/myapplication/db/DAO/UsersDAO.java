package com.ppjava13v2.vera.myapplication.db.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ppjava13v2.vera.myapplication.db.UtilityCreateDatabase;
import com.ppjava13v2.vera.myapplication.db.entities.Users;

/**
 * provides CRUD-operations with users
 */
public class UsersDAO {

    private UtilityCreateDatabase utilityCreateDatabase;

    public UsersDAO() {
        utilityCreateDatabase=UtilityCreateDatabase.getInstance();
    }

    public long addUser (Users user) {

        SQLiteDatabase database=utilityCreateDatabase.getWritableDatabase();
        ContentValues cv= new ContentValues();
        cv.put(UtilityCreateDatabase.USER_EMAIL, user.getEmail());
        cv.put(UtilityCreateDatabase.USER_LOGIN, user.getLogin());
        cv.put(UtilityCreateDatabase.USER_PASSWORD,user.getPassword());
        long result=database.insert(UtilityCreateDatabase.TABLE_NAME, null, cv);

        return result;
    }


    public long deleteUser (long id) {

        SQLiteDatabase database=utilityCreateDatabase.getWritableDatabase();
        return database.delete (UtilityCreateDatabase.TABLE_NAME,UtilityCreateDatabase._ID+"= " + id,null);

    }

    public Cursor findByLoginAndPassword (String login,String password) {

        SQLiteDatabase database=utilityCreateDatabase.getWritableDatabase();
        String [] resultSet={UtilityCreateDatabase._ID,UtilityCreateDatabase.USER_EMAIL,UtilityCreateDatabase.USER_LOGIN,UtilityCreateDatabase.USER_PASSWORD};
        String selection = "login=? AND password=?";
        String [] selectionArgs={login,password};
        Cursor cursor=database.query(UtilityCreateDatabase.TABLE_NAME, resultSet, selection, selectionArgs, null, null, null, null);

        return cursor;
    }
}
