package com.ppjava13v2.vera.myapplication.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.*;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.ppjava13v2.vera.myapplication.R;
import com.ppjava13v2.vera.myapplication.utilities.ListURL;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Generates token for android device and sends data to server
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String SENT_TOKEN_TO_SERVER ="sentTokenToServer" ;
    public final static String SEND_GCM_TOKEN_URL = ListURL.SERVER_IP+ListURL.GCM_TOKEN_URL;
    private String login;
    private String password;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        login = intent.getStringExtra("login");
        password = intent.getStringExtra("password");

        boolean res=sharedPreferences.getBoolean(SENT_TOKEN_TO_SERVER,false);

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.wtf(TAG, "GCM Registration Token: " + token);

            sendRegistrationToServer(token);
            sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();

        } catch (IOException e) {

            Log.d(TAG, "Failed to complete token refresh", e);
            sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, false).apply();
        }

    }


    public void sendRegistrationToServer (final String token) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection connection=null;
                try {

                    URL url = new URL(SEND_GCM_TOKEN_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                    String userpass = login+":"+password;
                    String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
                    connection.setRequestProperty("Authorization", basicAuth);
                    connection.connect();

                    OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());

                    wr.write("token="+token);

                    wr.flush();
                    wr.close();
                    int responseCode = connection.getResponseCode();

                    if (responseCode==200) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String response = reader.readLine();
                        System.out.println(response);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                finally {
                    connection.disconnect();
                }
                return null;
            }
        }.execute();
    }
}
