package com.ppjava13v2.vera.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.ppjava13v2.vera.myapplication.services.*;


public class RegistrationActivity extends Activity {

    private UserService userService;
    private EditText mEmail;
    private EditText mLogin;
    private EditText mPassword;
    private ProgressBar mProgressBar;
    private SendUserDataToServer sendUserDataToServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.baloons);
        setContentView(R.layout.activity_registration);

        userService=new UserService ();
        mEmail= (EditText) findViewById(R.id.email);
        mLogin= (EditText) findViewById(R.id.user_login);
        mPassword= (EditText) findViewById(R.id.user_password);
        mProgressBar= (ProgressBar) findViewById(R.id.registrationProgressBar);
    }

    public void onStartLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void onSaveData(View view) {

        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        String email=mEmail.getText().toString();
        String login=mLogin.getText().toString();
        String password=mPassword.getText().toString();

        if(login.length() == 0) {
            mLogin.setError("First name is required!");
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
        }

        if(password.length() == 0) {
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            mPassword.setError("First name is required!");
        }


        if(password.length()<=6) {
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            mPassword.setError("Your password must be at least 6 characters in length" );
        }



        if (login.length() != 0&&password.length()!=0&&password.length()>6) {

            long result=userService.saveUser(email,login,password);

            if (result!=-1&&isNetworkConnected ()) {
                sendUserDataToServer=new SendUserDataToServer (login,password);
                sendUserDataToServer.sendUserData(mProgressBar,this,result);
            }
            else if (!isNetworkConnected ()) {
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
            }

            else {
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                Toast.makeText(this, "Check your input data", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
