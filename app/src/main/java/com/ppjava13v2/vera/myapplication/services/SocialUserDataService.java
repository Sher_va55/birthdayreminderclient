package com.ppjava13v2.vera.myapplication.services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.ppjava13v2.vera.myapplication.FileAdapter;
import com.ppjava13v2.vera.myapplication.db.entities.SocialUser;
import com.ppjava13v2.vera.myapplication.utilities.ListURL;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SocialUserDataService {


    private String login;
    private String password;

    public SocialUserDataService() {
    }

    public SocialUserDataService(String login, String password) {
        this.login = login;
        this.password = password;
    }

    private final static String SEND_TOKEN_URL = ListURL.SERVER_IP+ListURL.FACEBOOK_TOKEN_URL;
    private final static String GET_FRIENDS_URL = ListURL.SERVER_IP+ListURL.GET_FRIENDS;
    private final static String SEND_CUSTOM_USER = ListURL.SERVER_IP+ListURL.CUSTOM_USER;
    private final static String SAVE_FRIENDS_URL = ListURL.SERVER_IP+ListURL.SAVE_FRIENDS;


    public void sendFacebookAccessToken(final String accessToken) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    URL url = new URL(SEND_TOKEN_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                    String userpass = login+":"+password;
                    String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
                    connection.setRequestProperty("Authorization", basicAuth);
                    connection.connect();

                    OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());

                    wr.write("accessToken=" + accessToken);

                    wr.flush();
                    wr.close();
                    int responseCode = connection.getResponseCode();

                    if (responseCode==200) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String response = reader.readLine();
                        System.out.println("send facebook access token "+response);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public List<SocialUser> getFriends(final ProgressBar progressBar, final List<SocialUser> friends, final FileAdapter fileAdapter) {

        AsyncTask<Void, Void, List<SocialUser>> task = new AsyncTask<Void, Void, List<SocialUser>>() {


            @Override
            protected List<SocialUser> doInBackground(Void... params) {

                HttpURLConnection connection=null;
                try {

                    URL url = new URL(GET_FRIENDS_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(15000);

                    String userpass = login+":"+password;
                    String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
                    connection.setRequestProperty("Authorization", basicAuth);

                    int httpStatus = connection.getResponseCode();
                    InputStream is = connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String facebookData = br.readLine();

                    GsonBuilder builder = new GsonBuilder();

                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    List<SocialUser> socialUser =gson.fromJson(facebookData, new TypeToken<List<SocialUser>>() {
                    }.getType());

                    return socialUser;

                } catch (java.net.SocketTimeoutException|java.net.ConnectException e) {
                    e.printStackTrace();
                    Log.e("connection", "connection refused method getFriends");


                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }

                return null;
            }

            @Override
            protected void onPostExecute(List<SocialUser> socialUsers) {
                super.onPostExecute(socialUsers);
                progressBar.setVisibility(ProgressBar.INVISIBLE);

                if (socialUsers!=null) {
                    friends.clear();
                    friends.addAll(socialUsers);
                    fileAdapter.notifyDataSetChanged();
                }
            }

        }.execute();

        return null;
    }


    public void sendCustomUserToServer(final String fio, final String birthday) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    URL url = new URL(SEND_CUSTOM_USER);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                    String userpass = login+":"+password;
                    String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
                    connection.setRequestProperty("Authorization", basicAuth);
                    connection.connect();

                    OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("FIO", fio)
                            .appendQueryParameter("birthday", birthday);

                    String query = builder.build().getEncodedQuery();

                    wr.write(query);
                    wr.flush();
                    wr.close();
                    int responseCode = connection.getResponseCode();
                    System.out.println("test+  "+responseCode);

                    if (responseCode==200) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String response = reader.readLine();
                        System.out.println(response);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();

    }


    public void refreshFacebookFriends() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection connection=null;
                try {

                    URL url = new URL(SAVE_FRIENDS_URL);
                    connection = (HttpURLConnection) url.openConnection();

                    String userpass = login+":"+password;
                    String basicAuth = "Basic " + Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
                    connection.setRequestProperty("Authorization", basicAuth);

                    int httpStatus = connection.getResponseCode();
                    InputStream is = connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String data = br.readLine();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }

                return null;
            }
        }.execute();
    }
}
