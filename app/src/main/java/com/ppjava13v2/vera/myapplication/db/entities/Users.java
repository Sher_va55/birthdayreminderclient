package com.ppjava13v2.vera.myapplication.db.entities;

/**
 * Contains data about user - email,login and password
 */
public class Users {

    private long id;
    private String email;
    private String login;
    private String password;

    public Users(String email, String login, String password) {
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
