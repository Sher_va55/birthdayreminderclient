package com.ppjava13v2.vera.myapplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Manages operations with database
 */
public class UtilityCreateDatabase extends SQLiteOpenHelper implements BaseColumns {

    private static UtilityCreateDatabase instance;

    public static final  String NAME_DATABASE ="BirthdayReminder" ;
    public static final  String TABLE_NAME ="Users" ;
    public static final  String USER_EMAIL ="email" ;
    public static final  String USER_LOGIN ="login";
    public static final  String USER_PASSWORD ="password" ;

    private UtilityCreateDatabase(Context context) {

        super(context, NAME_DATABASE, null, 1);
    }

    public static void init(Context context) {
        instance = new UtilityCreateDatabase(context);
    }

    public static UtilityCreateDatabase getInstance() {
        if(instance == null) {
            throw new IllegalStateException("My test exception");
        }

        return instance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY,%s TEXT, %s TEXT,%s TEXT)", TABLE_NAME, _ID,USER_EMAIL,USER_LOGIN,USER_PASSWORD);
        db.execSQL (query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
