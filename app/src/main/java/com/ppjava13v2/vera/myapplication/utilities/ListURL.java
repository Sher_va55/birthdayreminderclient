package com.ppjava13v2.vera.myapplication.utilities;

/**
 * Contains URL for http requests
 */
public class ListURL {

    //public final static String SERVER_IP ="http://192.168.0.16:8080";

    public final static String SERVER_IP ="https://cryptic-sands-17543.herokuapp.com";
    public final static String GCM_TOKEN_URL = "/GCM/saveToken";
    public final static String USER_URL = "/auth";
    public final static String FACEBOOK_TOKEN_URL = "/facebook/token";
    public final static String GET_FRIENDS = "/facebook/get/friends";
    public final static String CUSTOM_USER = "/client/save/custom";
    public final static String SAVE_FRIENDS = "/facebook/save/friends";
}
