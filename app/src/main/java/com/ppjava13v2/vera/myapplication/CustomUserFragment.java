package com.ppjava13v2.vera.myapplication;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;


public class CustomUserFragment extends DialogFragment  implements DialogInterface.OnClickListener  {

    public interface CustomUserListener  {

        void sendCustomUser (EditText fio,EditText date);

    }

    private View mCustomUserView;
    private EditText name;
    private EditText birthday;
    private CustomUserListener mCustomUserListener;

    public CustomUserFragment() {
    }

    @SuppressLint("ValidFragment")
    public CustomUserFragment(CustomUserListener mCustomUserListener) {
        this.mCustomUserListener = mCustomUserListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        mCustomUserView =inflater.inflate (R.layout.custom_user_fragment,null);

        name= (EditText) mCustomUserView.findViewById(R.id.custom_name);
        birthday=(EditText) mCustomUserView.findViewById(R.id.custom_birthday);

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    DialogFragment newFragment = new DateBirthdayFragment(birthday);
                    newFragment.show(getFragmentManager (),"DatePicker");

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity());
        builder.setTitle("Add friend")
                .setView(mCustomUserView)
                .setPositiveButton("Save",this)
                .setNegativeButton("Cancel",this);


        return builder.create ();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {

            case DialogInterface.BUTTON_POSITIVE:
                mCustomUserListener.sendCustomUser(name,birthday);
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
    }
}
