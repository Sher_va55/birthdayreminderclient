package com.ppjava13v2.vera.myapplication.services;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ppjava13v2.vera.myapplication.utilities.ListURL;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Sends post request to server with parameters login and password
 */
public class SendUserDataToServer {

    private String login;
    private String password;
    private Integer mResponseCode;
    private UserService userService;

    public SendUserDataToServer() {
        userService=new UserService ();
    }

    public SendUserDataToServer(String login, String password) {
        this.login = login;
        this.password = password;
        userService=new UserService ();
    }

    public final static String SEND_USER_URL = ListURL.SERVER_IP+ListURL.USER_URL;

    public void sendUserData (final ProgressBar mProgressBar,
                              final Context context,final long id) {

        AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {

            @Override
            protected Integer  doInBackground(Void... params) {
                HttpURLConnection connection=null;
                try {

                    URL url = new URL(SEND_USER_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                    connection.connect();

                    OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("login", login)
                            .appendQueryParameter("password", password);

                    String query = builder.build().getEncodedQuery();

                    wr.write(query);
                    wr.flush();
                    wr.close();
                    mResponseCode = connection.getResponseCode();


                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }
                return mResponseCode;
            }

            @Override
            protected void onPostExecute(Integer responseCode) {
                super.onPostExecute(responseCode);
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);

                if (mResponseCode==200) {
                    Toast.makeText(context, "Data saved successfully", Toast.LENGTH_SHORT).show();
                }
                else if (mResponseCode==500) {
                    userService.deleteUser(id);
                    Toast.makeText(context, "User has already exists", Toast.LENGTH_SHORT).show();
                }
                else {
                    userService.deleteUser(id);
                    Toast.makeText(context, "Try repeat later", Toast.LENGTH_SHORT).show();
                }

            }
        }.execute();

    }
}
