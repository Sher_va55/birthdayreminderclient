package com.ppjava13v2.vera.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ppjava13v2.vera.myapplication.services.UserService;


public class LoginActivity extends Activity {

    private UserService userService;
    private EditText user_login;
    private EditText user_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.baloons);
        setContentView(R.layout.activity_login);
        userService=new UserService ();
        user_login= (EditText) findViewById(R.id.user);
        user_password=(EditText) findViewById(R.id.password);
    }

    public void onDisplayFriendsList(View view) {

        String login=user_login.getText().toString();
        String password=user_password.getText().toString();

        if (userService.checkPassword(login,password)) {
            Intent intent = new Intent(this, FriendsBirthdayActivity.class);
            intent.putExtra("login",login);
            intent.putExtra("password",password);
            startActivity(intent);
            finish();
        }
        else {
            Toast.makeText(this, "Login or password incorrect", Toast.LENGTH_LONG).show();
        }

    }

    public void onStartRegistration(View view) {
        startActivity(new Intent(this, RegistrationActivity.class));
        finish();
    }



}
