package com.ppjava13v2.vera.myapplication.services;

import android.app.*;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;


import com.google.android.gms.gcm.GcmListenerService;
import com.ppjava13v2.vera.myapplication.MainActivity;
import com.ppjava13v2.vera.myapplication.R;

import java.util.Random;

/**
 * Service used for receiving GCM messages. When a message is received this service will log it.
 */
public class GcmService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";


    @Override
    public void onMessageReceived(String from, Bundle data) {

        String message = data.getString("message");
        String title=data.getString("title");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        sendNotification("Received GCM Message: " + message,title);
    }

    @Override
    public void onDeletedMessages() {
        sendNotification("Deleted messages on server","");
    }


    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg,String title) {

        NotificationManager mNotificationManager= (NotificationManager) getSystemService (NOTIFICATION_SERVICE);
        Notification.Builder mBuilder = new Notification.Builder (this);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        final int not_nu=generateRandom();
        PendingIntent pendingIntent=PendingIntent.getActivity(this, not_nu, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_donkey);

        mBuilder.setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_donkey)
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent);

        Notification notification = new Notification.BigTextStyle(mBuilder)
                .bigText(msg).build();

        mNotificationManager.notify(not_nu, mBuilder.build());
    }

    public int generateRandom(){
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}
