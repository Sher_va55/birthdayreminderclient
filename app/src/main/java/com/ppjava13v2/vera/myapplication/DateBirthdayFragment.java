package com.ppjava13v2.vera.myapplication;

import android.annotation.SuppressLint;
import android.app.*;
import android.os.Bundle;
import android.widget.*;

import java.util.Calendar;


public class DateBirthdayFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private EditText birthday;

    public DateBirthdayFragment() {
    }

    @SuppressLint("ValidFragment")
    public DateBirthdayFragment(EditText birthday) {
        this.birthday = birthday;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        int correctedMonth=monthOfYear+1;
        String date=dayOfMonth + "/"+ correctedMonth+"/"+year;
        birthday.setText(date);
    }
}
