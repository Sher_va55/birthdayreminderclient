package com.ppjava13v2.vera.myapplication;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.facebook.*;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.google.android.gms.common.*;
import com.ppjava13v2.vera.myapplication.db.entities.SocialUser;
import com.ppjava13v2.vera.myapplication.services.*;

import java.util.ArrayList;
import java.util.List;


public class FriendsBirthdayActivity extends Activity
        implements AdapterView.OnItemClickListener, CustomUserFragment.CustomUserListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "FriendsBirthdayActivity";

    private String login;
    private String password;
    private SocialUserDataService socialUserDataService;
    private ListView friendsList;
    private List<SocialUser> friends;
    private ProgressBar progressBar;
    private MessageDialog messageDialog;
    private CallbackManager callbackManager;
    private FileAdapter fileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.baloons);

        setContentView(R.layout.activity_friends_birthday);

        progressBar= (ProgressBar) findViewById(R.id.progressBar);

        Intent intent = getIntent();
        login = intent.getStringExtra("login");
        password = intent.getStringExtra("password");

        callbackManager = CallbackManager.Factory.create();
        messageDialog=new MessageDialog (this);

        socialUserDataService = new SocialUserDataService(login,password);
        friendsList= (ListView) findViewById(R.id.list);
        friends=new ArrayList<>();
        fileAdapter= new FileAdapter(this,R.layout.viewforfriendslist,friends);
        friendsList.setAdapter(fileAdapter);
        friendsList.setOnItemClickListener(this);
        showListView();


        if (checkPlayServices()) {

            Intent registerIntent = new Intent(this, RegistrationIntentService.class);
            registerIntent.putExtra("login", login);
            registerIntent.putExtra("password", password);
            startService(registerIntent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        menuInflater.inflate(R.menu.menu_friends_birthday, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.exit) {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        if (item.getItemId()==R.id.add) {
            Intent facebookIntent= new Intent (this, FacebookActivity.class);
            facebookIntent.putExtra("login", login);
            facebookIntent.putExtra("password", password);
            startActivity(facebookIntent);
            finish();
        }

        if (item.getItemId()==R.id.refresh) {
            socialUserDataService.refreshFacebookFriends();
            showListView();
        }

        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        messageDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("successSend", "success");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("errorShare", "error");
            }

            @Override
            public void onCancel() {
                Log.d("cancelSend", "cancel");
            }
        });

        if (MessageDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder().build();
            messageDialog.show(linkContent);
        }
    }


    public void onShowCustomUserDialog(View view) {

        DialogFragment authDialog = new CustomUserFragment(this);
        authDialog.show(getFragmentManager(), "customTag");
    }

    @Override
    public void sendCustomUser(EditText fio, EditText date) {
        socialUserDataService.sendCustomUserToServer(fio.getText().toString(), date.getText().toString());
        showListView();
    }

    public void showListView() {

        if (progressBar.getVisibility()==ProgressBar.INVISIBLE)
            progressBar.setVisibility(ProgressBar.VISIBLE);

        socialUserDataService.getFriends(progressBar,friends,fileAdapter);

    }
}
